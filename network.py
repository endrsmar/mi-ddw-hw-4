import networkx as nx
import matplotlib.pyplot as plt

def insert_actors(G, actors, movie):
    for a in actors:
        if not G.has_node(a):
            G.add_node(a)
        for b in actors:
            if not G.has_node(b):
                G.add_node(b)
            if a != b and not G.has_edge(a, b):
                G.add_edge(a, b, attr_dict={"movie": movie})

def kevin_bacon_numbers(G):
    for actor,path in nx.shortest_path(G, target='Kevin Bacon').items():
        G.node[actor]['kevin_bacon_number'] = len(path)

currentMovie = ""
connectedActors = []
G = nx.Graph()

with open('casts.csv') as f:
    for line in f:
        cols = line.split(';')
        if currentMovie != cols[1][1:-1]:
            insert_actors(G, connectedActors, currentMovie)
            currentMovie = cols[1][1:-1]
            connectedActors = []
        if cols[2][1:-1] and cols[2][1:-1] != "s a":
            connectedActors.append(cols[2][1:-1])
    insert_actors(G, connectedActors, currentMovie)

degreeSum = 0
for n in G.nodes():
    degreeSum += G.degree(n)

with open('statistics.csv', 'w') as f:
    f.write("Metric,Value\n")
    f.write("Node count,"+str(len(G.nodes()))+"\n")
    f.write("Edge count,"+str(len(G.edges()))+"\n")
    f.write("Avg. degree,"+str(degreeSum/float(len(G.nodes())))+"\n")
    f.write("Density,"+str(nx.density(G))+"\n")
    f.write("Connected components,"+str(nx.number_connected_components(G))+"\n")
    f.write("Connectivity,"+str(nx.node_connectivity(G))+"\n")

kevin_bacon_numbers(G)

centralities = [nx.degree_centrality, nx.closeness_centrality, nx.betweenness_centrality, nx.eigenvector_centrality]

for centrality in centralities:
    scores = centrality(G)
    for actor in sorted(scores, key=scores.get, reverse=True):
        G.node[actor][centrality.__name__] = scores[actor]

attributes = ['degree_centrality','closeness_centrality','betweenness_centrality','eigenvector_centrality','kevin_bacon_number']
with open('nodes.csv', 'w') as nf:
    with open('edges.csv', 'w') as ef:
        nf.write("Id;"+";".join(attributes)+"\n")
        ef.write("Source;Target;Type;Movie\n")
        for node in G.nodes(data=True):
            nf.write(node[0]+";"+";".join([str(v) for v in node[1].values()])+"\n")
            for n in G.neighbors(node[0]):
                ef.write(node[0]+";"+n+";Undirected;"+G[node[0]][n]['movie']+"\n")

communities = {cid+1:community for cid,community in enumerate(nx.k_clique_communities(G, 5))}
    for community in sorted(communities, key = lambda x: len(communities[x]), reverse=True)[:5]:
        for actor in communities[community]:
            nf.write(actor+";"+str(community)+"\n")
            for partner in G.neighbors(actor):
                f.write(actor+";"+partner+";Undirected\n")

